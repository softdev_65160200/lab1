/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;
import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab1 {
    
    public static void main(String[] args) {
        System.out.println("Welcome To TicTacToe");
        playTic();
        System.out.println("___________________________________");
    }
    
    static char[][] tictac = {{'-','-','-'},
                              {'-','-','-'},
                              {'-','-','-'}};
    static int row, col;
    static char player = 'X';
    static String play;
    static int nowTurn;
    public static void clear(){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
            tictac[i][j] = '-';
            }
        }
    }
    
    public static void showTable(){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
            System.out.print(tictac[i][j] + " " );
            }
            System.out.println();
        }
    }
    public static void inputRowCol() {
            Scanner sc = new Scanner(System.in);
            System.out.print("Turn " + player + " Please input Row Col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (checkAlready(row, col)) {
                System.out.println("This spot alreay have tic");
                showTable();
                System.out.println("Please input RowCol again");
                inputRowCol();
            }
            tictac[row][col] = player;  
    } 
    public static void switchPlayer() {
        if (player == 'X') player = 'O';
        else player = 'X';
    }
       static boolean checkWinRow() {
        for (int c = 0; c < 3; c++) {
            if (tictac[row][c] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkWinCol() {
        for (int r = 0; r < 3; r++) {
            if (tictac[r][col] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCross1() {
        return tictac[0][0] == player && tictac[1][1] == player && tictac[2][2] == player;
    }

    static boolean checkCross2() {
        return tictac[0][2] == player && tictac[1][1] == player && tictac[2][0] == player;
    }
    
    static boolean checkWin() {
        return checkWinCol() || checkWinRow() || checkCross1() || checkCross2();
    }
    
    static boolean checkAlready(int Row, int Col) {
        if (tictac[Row][Col] == 'X' || tictac[Row][Col] == 'O') return true;
        return false;
    }
    
    static void continueGame() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Continune? (Y/N): ");
        play = sc.next();
        String now = play.toUpperCase();
        if (now.equals("Y")) {
            System.out.println("Welcome To TicTacToe");
            clear();
            playTic();
        }
    }
    
    public static void playTic(){
        for (int countTurn = 1; countTurn < 10; countTurn++) {
            showTable();
            inputRowCol();
            if (checkWin()) {
                showTable();
                System.out.println("Congratulation Player " + player + " Win!?!");
                break;
            }
            if (countTurn == 9) {
                showTable();
                System.out.println("Draw!?!");
            }
            switchPlayer();
            
        }
        continueGame();
    }
}
